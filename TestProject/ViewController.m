//
//  ViewController.m
//  TestProject
//
//  Created by yangzhen10 on 2019/3/19.
//  Copyright © 2019 yangzhen. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
    
    // Do any additional setup after loading the view, typically from a nib.
}


@end
