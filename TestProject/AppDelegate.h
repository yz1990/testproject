//
//  AppDelegate.h
//  TestProject
//
//  Created by yangzhen10 on 2019/3/19.
//  Copyright © 2019 yangzhen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

