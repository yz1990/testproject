//
//  main.m
//  TestProject
//
//  Created by yangzhen10 on 2019/3/19.
//  Copyright © 2019 yangzhen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
